# -*- coding: utf-8 -*-
import asyncio
import threading
import time

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.
from PyQt6.QtCore import QUrl

import sys
import os
import requests
from PyQt6.QtWidgets import QApplication, QMainWindow, QSystemTrayIcon
from PyQt6.uic import loadUi
from PyQt6.QtNetwork import QNetworkAccessManager, QNetworkRequest, QNetworkReply
from PyQt6 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def __init__(self):
        self.url = None
        self.name = None
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(826, 322)
        MainWindow.setStyleSheet("background-color:black;\n"
                                 "color:white;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.spinBox_2 = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox_2.setGeometry(QtCore.QRect(15, 185, 801, 46))
        self.spinBox_2.setStyleSheet("font-size:24px;border:1px solid white;")
        self.spinBox_2.setObjectName("spinBox_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(15, 125, 796, 51))
        self.label_3.setStyleSheet("font-size:36px;")
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(15, 15, 796, 41))
        self.label_4.setAutoFillBackground(False)
        self.label_4.setStyleSheet("font-size:36px;")
        self.label_4.setObjectName("label_4")
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(15, 65, 801, 41))
        font = QtGui.QFont()
        font.setPointSize(-1)
        self.textEdit_2.setFont(font)
        self.textEdit_2.setStyleSheet("font-size:24px;")
        self.textEdit_2.setDocumentTitle("")
        self.textEdit_2.setObjectName("textEdit_2")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(15, 250, 801, 56))
        self.pushButton_2.setStyleSheet("font-size:34px; border:1px solid white;")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.download_zip_file)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Random image download"))
        self.label_3.setText(_translate("MainWindow", "Выберите количетсво картинок для скачивания"))
        self.label_4.setText(_translate("MainWindow", "Укажите путь установки (скачивания)"))
        self.textEdit_2.setPlaceholderText(_translate("MainWindow", "URL"))
        self.pushButton_2.setText(_translate("MainWindow", "Скачать"))

    def download_img(self, url, name):
        response = requests.get(url)
        if response.status_code == 200:
            print(200)
            with open(f'images/png{name + time.time()}.png', 'wb') as f:
                f.write(response.content)
        else:
            print('error')


    def download_zip_file(self):
        print('download_zip_file')
        count_ = self.spinBox_2.text()
        response = requests.get(f'http://127.0.0.1:8000/get_urls/{count_}')
        json_data = response.json()
        for img in json_data['imgs']:
            threading.Thread(target=self.download_img, args=(img, json_data['imgs'].index(img))).start()

